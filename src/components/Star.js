import React from 'react'

const Star = ({isSelected, index, hoverIndex, onClick}) => {
    let isStarSelected = false
    if(hoverIndex === -1) {
        isStarSelected = isSelected
    } else if(index <= hoverIndex) {
        isStarSelected = true
    }
    console.log({hoverIndex, isStarSelected})
    const handleClick = () => onClick(index)
    return <div data-element="star" data-index={index} className={`star ${isStarSelected ? '__selected' : ''}`} onClick={handleClick}/>
}

export default Star