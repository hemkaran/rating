import React, { useState } from "react";
import Star from "./Star";
import "./Rating.css";

// <Rating numberOfStars={5} selectedStars={2} onChange={handleRatingChange} />

const Rating = (props) => {
  const [hoverIndex, setHoverIndex] = useState(-1);
  const { numberOfStars = 5, selectedStars, onChange } = props;
  const arr = new Array(numberOfStars).fill(0);

  const handleMouseOver = (event) => {
    const { element, index } = event.target.dataset;
    if (element === "star") {
      setHoverIndex(index);
    }
  };

  const handleMouseLeave = (event) => {
    setHoverIndex(-1)
  };
  const handleClick = (index) => onChange(index+1)

  console.log('selectedStars ', selectedStars)
  return (
    <section
      className="rating__container"
      onMouseOver={handleMouseOver}
      onMouseLeave={handleMouseLeave}
    >
      {arr.map((item, index) => (
        <Star
          key={index}
          index={index}
          isSelected={index < selectedStars}
          hoverIndex={hoverIndex}
          onClick={handleClick}
        />
      ))}
    </section>
  );
};

export default Rating;
