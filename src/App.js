import React, {useState} from 'react';
import Rating from './components/Rating.js'
import './App.css';

function App() {
  const [selectedStars, setSelectedStars] = useState(0)
  const onChange = (index) => setSelectedStars(index)
  return (
    <div className="App">
      <span>Learn react by creating Rating component</span>
      <Rating numberOfStars={5} selectedStars={selectedStars} onChange={onChange}/>
    </div>
  );
}

export default App;
